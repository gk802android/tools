#!/bin/bash -x

SUDO="sudo -E"

if [ -z "$ANDROID_PRODUCT_OUT" ]; then
    echo "Error. lunch not configured."
    echo "please run . build/envsetup.sh"
    echo "and the configure product using lunch"
    exit 127
fi

UBOOT_IMAGE=$ANDROID_PRODUCT_OUT/u-boot-6q.bin
KERNEL_IMAGE=$ANDROID_PRODUCT_OUT/uImage
RAMDISK_IMAGE=$ANDROID_PRODUCT_OUT/uramdisk.img
SYSTEM_IMAGE=$ANDROID_PRODUCT_OUT/system.img

do_format=yes
DEVICE=/dev/sdb
echo -n "SDcard device ($DEVICE): "
read INPUTDEV
if [ -z $INPUTDEV ]; then
    echo empty
else
    echo $INPUTDEV
fi

if cat /proc/mounts | grep -q `basename $DEVICE`; then
    umount ${DEVICE}?
fi

if [ "$do_format" == "yes" ]; then
# wipe mbr
    $SUDO dd if=/dev/zero of=${DEVICE} conv=fsync bs=512 count=2047

   spacing=32
   sd_size=`$SUDO sfdisk -s ${DEVICE}`
   sd_size=`expr ${sd_size} / 1024`
   boot_start=1
   boot_size=16
   recovery_start=`expr ${boot_start} + ${boot_size}`
   recovery_size=32

   extpart_start=`expr ${recovery_start} + ${recovery_size}`
   extpart_size=`expr ${sd_size} - ${boot_size} - ${recovery_size} - ${spacing}`
   extpart_end=`expr ${extpart_start} + ${extpart_size}`

   system_size=512
   cache_size=512
   vendor_size=8
   misc_size=8

   # data will fill up whatever is left
   data_start=`expr ${extpart_start}`
   data_start_align=`expr ${data_start} \% 8`
   data_start=`expr ${data_start} - ${data_start_align} + 8`
   data_size=`expr ${extpart_size} - ${system_size} - ${cache_size} - ${vendor_size} - ${misc_size} - ${spacing} - 16`
   data_align=`expr ${data_size} \% 8`
   data_size=`expr ${data_size} - ${data_align}`
   data_end=`expr ${data_start} + ${data_size}`

   system_start=${data_end}
   system_end=`expr ${system_start} + ${system_size}`

   cache_start=${system_end}
   cache_end=`expr ${cache_start} + ${cache_size}`

   # format card
   $SUDO parted -s $DEVICE mklabel msdos
   $SUDO parted -s -a optimal $DEVICE mkpart primary ${boot_start} ${recovery_start}
   $SUDO parted -s -a optimal $DEVICE mkpart primary ${recovery_start} ${extpart_start}
   $SUDO parted -s -a optimal $DEVICE mkpart extended ${extpart_start} ${extpart_end}
   $SUDO parted -s -a optimal $DEVICE mkpart logical ${data_start} ${data_end}
   $SUDO parted -s -a optimal $DEVICE mkpart logical ${system_start} ${system_end}
   $SUDO parted -s -a optimal $DEVICE mkpart logical ${cache_start} ${cache_end}
   
   # bootloader
   $SUDO dd if=$UBOOT_IMAGE of=${DEVICE} conv=fsync bs=512 seek=2 skip=2

   # boot partition
   $SUDO mkfs.ext2 ${DEVICE}1
fi


$SUDO mount -t ext2 ${DEVICE}1 /mnt
$SUDO rm -fr /mnt/*
$SUDO mkdir -p /mnt/boot
$SUDO cp $KERNEL_IMAGE /mnt/boot
$SUDO cp $RAMDISK_IMAGE /mnt/boot
$SUDO umount /mnt

#data
$SUDO mkfs.ext4 ${DEVICE}5
#system
UNSPARSE_IMG=`mktemp /tmp/system.img_unsparse_XXXX`
simg2img $SYSTEM_IMAGE $UNSPARSE_IMG
$SUDO dd if=$UNSPARSE_IMG of=${DEVICE}6 conv=fsync
rm -f $UNSPARSE_IMG
#cache
$SUDO mkfs.ext4 ${DEVICE}7

sync

if cat /proc/mounts | grep -q `basename $DEVICE`; then
    umount ${DEVICE}?
fi
